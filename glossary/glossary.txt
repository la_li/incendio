norma	κανόνας
incendio	πυρκαγιά
extinción	κατάσβεση
viento	άνεμος
dirección	κατεύθυνση
herramienta	εργαλείο
medio	μέσο
contrafuego	αντιπύρ
empleo	χρήση
ataque directo	άμεση προσβολή
ataque indirecto	έμμεση προσβολή
línea de defensa	γραμμή άμυνας
fase	Φάση
fuego	φωτιά
desplazamiento	μετατόπιση
aire	αέρας
llama	φλόγα	χιχιχι
bate fuegos	χτυπητήρι
actuación	δράση
actuación	διαδικασία
enfriamiento	ψύξη
aislamiento	μόνωση
controlado	ελεγχόμενη
controlado	υπό έλεγχο
activo	ενεργή
extinguido	σβησμένη
objetivo	σκοπός
batefuegos	χτυπητήρι
combustible	καύσιμο
combustible	καύσιμη ύλη
ignición	ανάφλεξη
apertura	άνοιγμα
apertura	κατασκευή
faja	ζώνη
tierra	χώμα
cortafuegos	αντιπυρικό τείχος
disminución	μείωση
capacidad de arder	δυνατότητα καύσης
retardantes	επιβραδυντικά
frente	μέτωπο
línea de control	γραμμή ελέγχου
rastrillo	τσουγκράνα
quemada	καμένη
huida	διαφυγή
desprendimiento	κατολίσθηση
peligro	κίνδυνος
quema	κάψιμο
quema	καύση
rastrojo	καλαμιά
pasto	χορτάρι
bosque	δάσος
forestal	δασική
directo	άμεσο
indirecto	έμμεσο
sustancia	ουσία
se mezcla	αναμιγνύεται
se mezcla	ανακατεύεται
calor	θερμότητα
calor	ζέστη
oxígeno	οξυγόνο
alcanzar	φτάνω
aparición	εμφάνιση
vegetal	φυτικό
foco	εστία
causa	αιτία
causa	αίτιο
naturale	φυσικό
rayo	κεραυνός
negligente	αμέλεια
intencionada	πρόθεση
accidental	καταλάθος
transmisión	μεταφορά
propagación	διάδοση
convección	επαγωγή
radiación	ακτινοβολία
conducción	επαφή
interior	εσωτερικό
cuerpo	σώμα
molécula	μόριο
compone	αποτελεί
raíz	ρίζα
tronco	κορμός
rama	κλαδί
ramas	κλαδιά
raíces	ρίζες
vegetación	βλάστηση
de subsuelo	υπόγειες
de copas	επικόρυφες
de superficie	έρπουσες
de superficie	εδάφους
matorral	φρύγανο
leña	ξύλο
hojarasca	πεσμένα φύλλα
suelo	χώμα
materia orgánica	οργανική ύλη
seca	ξερή
seca	ξηρή
suelo	έδαφος
humo	καπνός
cabeza	κεφαλή
flanco	πλευρό
borde	περίγραμμα
lateral	πλευρικό
cola	ουρά
despacio	αργά
comportamiento	συμπεριφορά
factor	παράγοντας
vivo	ζωντανό
muerto	νεκρό
cantidad	ποσότητα
densidad	πυκνότητα
humedad	υγρασία
estratificación	διαστρωμάτωση
ligero	ελαφρύ
pesado	βαρύ
combustibilidad	ευφλεκτότητα
grado	βαθμός
arbustiva	θαμνοειδής
distribución	κατανομή
continua	συνεχής
discontinua	ασυνεχής
continuidad	συνέχεια
contenido	περιεκτικότητα
punto	σημείο
altura	ύψος
riesgo	κίνδυνος
temperatura	θερμοκρασία
medida	μέτρο
estaciones	εποχές
vientos de ladera	άνεμοι πλαγιάς
solana	ηλιοφάνεια
umbría	συννεφιά
frecuencia	συχνότητα
velocidad	ταχύτητα
vientos de valle	άνεμοι κοιλάδας
valle	κοιλάδα
corriente	ρεύμα
método	μέθοδος
seguridad	ασφάλεια
accidente	ατύχημα
paso	βήμα
aislamiento	απομόνωση
zona	ζώνη
fenómeno	φαινόμενο
terreno	έδαφος
predicción	πρόβλεψη
hierba	χόρτο
arbusto	θάμνος
mata	θαμνάκι
tocon	κούτσουρο
mata	υποθάμνος
hoja	φύλλο
acícula	πευκοβελόνα
arboleda	συστάδα δέντρων
soto bosque	υπόροφος
soto bosque	χαμηλή βλάστηση
matorral	θαμνότοποι
mata	φρύγανο
mata	φρύγανο
topografía	τοπογραφία
clima	κλίμα
estado	κατάσταση
clasificación	ταξινόμηση
lento	αργό
escaso	ελάχιστο
desprendimiento	έκλυση
de forma aislada	ξεχωριστά
distinta	διαφορετική
el perímetro	η περίμετρος
circular	κυκλική
elipse	έλλειψη
decreciente	μειώνεται
intensidad	ένταση
residuos	σκουπίδια
acumulación	συσσώρευση
poda	κλάδεμα
corta	κοπή
despojos	υπολείμματα
limpio	καθαρό
material	υλικό
ladera	πλαγιά
acción	δράση
desecación	αποξήρανση
aportación	μεταφορά
atmosférica	ατμοσφαιρική
cresta	κορυφή
pendiente	πλαγιά
chispa	σπίθα
pavesa	κάρβουνο
personal	προσωπικό
agotado	εξαντλημένο
inmediato	άμεσο
